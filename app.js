console.log('This is a simple key/value store application\n');

//import and create the store object
var keyStore = require('./store.js');
var myStore = keyStore();


/// set variables for the server 
const http = require('http');
var url = require('url');
var port = 3000;


//create the server object 
const server = http.createServer().listen(port);


server.on('request',  (req, res) => {
    urlPath = url.parse(req.url, true).pathname;

    //if the path is list run the list function
    if(urlPath =='/list') {
        res.write(JSON.stringify(myStore.list()));
        res.end(); 
    
    //if the path is getInfo parse the key value and 
    // run the getInfo function
    }else if (urlPath == '/getInfo') {
        var query = url.parse(req.url, true).query;
        res.write(JSON.stringify(myStore.getInfo(query.key)));
        res.end(); 

    //if the path is setKey parse the key/value and 
    // run the setKey function
    }else if (urlPath == '/setKey') {
        var query = url.parse(req.url, true).query;
        res.write(JSON.stringify(myStore.setKey(query.key, query.value)));
        res.end(); 

    //if the path is setKey parse the key and 
    // run the remove function
    }else if (urlPath == '/remove') {
        var query = url.parse(req.url, true).query;
        res.write(JSON.stringify(myStore.remove(query.key)));
        res.end(); 
      
    //if the path is clear clear the store object 
    }else if (urlPath == '/clear'){
        res.write(JSON.stringify(myStore.clear()));
        res.end(); 
    
    //if the path is anything else return an error
    }else {
        res.write('<h1>Unknown Request!<h1>'); 
        res.end(); 
    }
   
});

console.log('Key store app is listenting on port ' + port);

