#!/usr/bin/env node
const http = require('http');
var usage = 'This is a simple key value store client\n';
usage += '***USAGE*** store [add mykey myvalue | list | get mykey | remove mykey | clear] \n';

//set connection variables
var options = {
    //host:'192.168.99.100',
    host: 'localhost',
    port: 3000,
    path: ''
};

//This function is used to make an update request 
//to the api, display if the request was successful
//or display an error
function callupdate(){
    var request = http.request(options, (res) => {
        if(res.statusCode == 200) {
            console.log('Update was successful.');
        }else {
            console.log('Error: Data not updated.');
        }
    });
    
    request.on('error', (err) => {
        console.log(`Error accessing ${options.host}:${options.port}`);
    });
    request.end();
};

//This function is used to make query call 
//to the api, display if the returned results
//or display an error
function callQuery() {
    var request = http.request(options, (res) => {
        res.on('data', (chunk) => {
          console.log('Data: ' + chunk);
        });
    });

    request.on('error', (err) => {
        console.log(`Error accessing ${options.host}:${options.port}`);
    });

    request.end();
}

var action = process.argv[2];

//check to validate input actions
if(action != 'add' && action != 'list' && action != 'get' && action != 'remove' && action != 'clear'){
    console.log('Unknown command: ' + action + '\n');
    console.log(usage);
} else {
    switch (action) {

        //This case will check that the user supplied a key 
        //and a value, make a call the the setKey function 
        case 'add':
            var key = process.argv[3].replace(/\s/g, '+');
            var value = process.argv[4].replace(/\s/g, '+');
            if (key == undefined || value == undefined) {
                console.log('You need to specify a key and value for this action. \n');
                console.log(usage);
            }else {
                options.path = '/setKey?key=' + key + '&value=' + value;
                callupdate();
            }
            break;

        //This case will make a call to the list function
        //and display the returning results    
        case 'list':
            options.path = '/list';
            callQuery();
            break;

        //This case will check that the user supplied a key, 
        //make a call the the getInfo function, send the key
        //and display the returning results
        case 'get':
            var key = process.argv[3].replace(/\s/g, '+');

            if (key == undefined) {
                console.log('You need to specify a key for this action. \n');
                console.log(usage);
            }else {
                key = key.replace(/\s/g, '+');
                options.path = '/getInfo?key=' + key;
                callQuery();
            }
            break;

        //This case will check that the user supplied a key, 
        //make a call the the remove function, send the key
        //and display whether the action was successful
        case 'remove':
            var key = process.argv[3].replace(/\s/g, '+');
            if (key == undefined) {
                console.log('You need to specify a key for this action. \n');
                console.log(usage);
            }else {
                options.path = '/remove?key=' + key;
                callupdate();             
            }
            break;

        //This case will call the clear function to remove
        //all data and will display whether the action was successful
        case 'clear':
            options.path = '/clear' ;
            callupdate();
            break;
        }
    }