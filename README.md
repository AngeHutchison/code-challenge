This is a simple key value store. 


### Start API
`$ npm start`
 
### Store CMDs

`$ store add mykey myvalue`

`$ store list`

`$ store get mykey`

`$ store remove mykey`

`$ store clear`

### Docker commands
`$ docker build -t ange-code-challenge . `

`$ docker run -p 3000:3000 -d ange-code-challenge`
