module.exports = function() {

    var fs = require('fs');
    var log = 'logfile';
    var obj = {};
    //this will pull in the intial data in the log file
    //if the log file exists
    if (fs.existsSync(log)) {
        console.log('Loading data from logfile');
        obj = JSON.parse(fs.readFileSync(log));
    }

    //This function will write to the log file
    function writetolog(func){
        data = JSON.stringify(obj);
            
        fs.writeFile(log, data, (err) => {  
            if (err) throw err;
            console.log('Data updated by ' + func);
        });
    }

    //store object fucntions
    var functions = {
    
        //This fucntion will add a new key/value to the store
        setKey: (newKey, newValue) => {
            obj[newKey] = newValue;
            writetolog('setKey');

            return obj;
        },
        
        //This fucntion will return a specific key/value
        //if the key exists or return an empy store
        getInfo: (searchKey) => {
            var temp = {};
            temp[searchKey] = obj[searchKey];
            return temp;
        },
        
        //This function will list all data in the store
        list: () => {
            return obj;
        },

        //This function will remove a key in the store
        remove: (key) => {
            delete obj[key];
            writetolog('remove');
            return obj;
        },

        //This function will clear all data in the store object
        clear: () =>{
            obj = {};
            writetolog('clear');
            return obj;
        }
    };
    return functions;
}