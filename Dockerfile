FROM node:carbon

# Create app directory
WORKDIR /usr/src/app

# Copy package files
COPY package*.json ./

# install dependencies 
RUN npm install

# Bundle app source
COPY . .

EXPOSE 3000

# command to start app
CMD [ "npm", "start" ]
